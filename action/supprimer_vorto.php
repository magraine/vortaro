<?php

/**
 * Action de suppression d'un mot de lexique
 *
 * @plugin     Vortaro - Dictionnaire et traductions
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Vortaro\Action
**/

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Effacer un mot de lexique
 *
 * @param null $id_vorto
 * @return void
 */
function action_supprimer_vorto_dist($id_vorto=null) {

	if (is_null($id_vorto)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_vorto = $securiser_action();
	}

	if (intval($id_vorto)) {
		sql_delete("spip_vortoj", "id_vorto=".intval($id_vorto));
		sql_delete("spip_auteurs_liens", "objet='vorto' AND id_objet=".intval($id_vorto));
	}

}
