<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'vortaro_description' => 'Ce plugin crée un lexique ou dictionnaire de mots, qui peuvent être traduits.',
	'vortaro_nom' => 'Vortaro - Dictionnaire et traductions',
	'vortaro_slogan' => 'Permet de créer un dictionnaire de traductions de mots',
);

?>