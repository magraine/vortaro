<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_vorto' => 'Ajouter ce mot de lexique',

	// E
	'explication_avant' => 'Indication affichée avant le titre de l\'entrée, par exemple «Un »',

	// I
	'icone_creer_vorto' => 'Créer un mot de lexique',
	'icone_modifier_vorto' => 'Modifier ce mot de lexique',
	'icone_supprimer_vorto' => 'Supprimer ce mot de lexique',
	'info_1_vorto' => 'Un mot de lexique',
	'info_aucun_vorto' => 'Aucun mot de lexique',
	'info_nb_vortoj' => '@nb@ mots de lexique',
	'info_vortoj_auteur' => 'Les mots de lexique de cet auteur',

	// L
	'label_avant' => 'Affichage avant le titre',
	'label_definition' => 'Définition',
	'label_titre' => 'Titre',
	'label_lang' => 'Langue',

	// R
	'retirer_lien_vorto' => 'Retirer ce mot de lexique',
	'retirer_tous_liens_vortoj' => 'Retirer tous les mots de lexique',

	// T
	'texte_ajouter_vorto' => 'Ajouter un mot de lexique',
	'texte_changer_statut_vorto' => 'Ce mot de lexique est :',
	'texte_creer_associer_vorto' => 'Créer et associer un mot de lexique',
	'titre_langue_vorto' => 'Langue de ce mot de lexique',
	'titre_logo_vorto' => 'Logo de ce mot de lexique',
	'titre_vorto' => 'Mot de lexique',
	'titre_vortoj' => 'Mots de lexique',
	'titre_vortoj_rubrique' => 'Mots de lexique de la rubrique',
	'texte_definir_comme_traduction_vorto' => 'Ce mot de lexique est une traduction du mot de lexique numéro :',
);

?>
