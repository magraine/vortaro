<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'restreindre_langue' => 'Restreindre la langue affichée',

	// T
	'tout_afficher' => 'Tout afficher',

	// V
	'vortaro_titre' => 'Vortaro - Dictionnaire et traductions',
);

?>
