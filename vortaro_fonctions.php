<?php

/**
 * Définit les fonctions du plugin Vortaro - Dictionnaire et traductions
 *
 * @plugin     Vortaro - Dictionnaire et traductions
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Vortaro\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


# pris du plugin tradsync
if (!function_exists('critere_traductions_dist')) {
/**
 * 
 * {traductions en} : tous les elements anglais etant des traductions
 * 	 = {!origine_traduction}{lang=en}
 *
 * {!traductions en} : tous les elements d'origine non traduits en anglais
 *
 */
function critere_traductions_dist($idb, &$boucles, $crit) {
	global $exceptions_des_tables;
	$not = $crit->not;
	$boucle = &$boucles[$idb];
	$primary = $boucle->primary;

	if (!$primary OR strpos($primary,',')) {
		erreur_squelette(_T('zbug_doublon_sur_table_sans_cle_primaire'), "BOUCLE$idb");
		return;
	}
	$_table = $boucle->id_table;
	$table = $boucle->type_requete;
	$table_sql = table_objet_sql(objet_type($table));

	$_id_trad = isset($exceptions_des_tables[$boucle->id_table]['id_trad']) ?
		$exceptions_des_tables[$boucle->id_table]['id_trad'] :
		'id_trad';



	if (isset($crit->param[0])){
		$_lang = calculer_liste($crit->param[0], array(), $boucles, $boucles[$idb]->id_parent);
	} else {
		#$_lang = '@$Pile[0]["lang"]';
		$_lang = "''";
		// rendons obligatoire ce parametre
		return (array('tradsync:zbug_critere_necessite_parametre', array('critere' => $crit->op )));
	}

	// base de {origine_traduction}
	$origine_traduction = array("'OR'",
			array("'='", "'$_table.$_id_trad'", "'$_table.$primary'"),
			array("'='", "'$_table.$_id_trad'", "'0'")
	);
	$lang = array("'='","'$_table.lang'", "sql_quote($_lang)");
	
	if ($not) {
		$boucle->where[] = $origine_traduction;
		$boucle->where[] = array("'NOT'", $lang);
		$boucle->where[] =
			array("'NOT'",
				array("'IN'", "'$_table.$primary'",
					"'('.sql_get_select( 'strad.$_id_trad', '$table_sql AS strad', 'strad.lang = ' . sql_quote($_lang)).')'"));
	} else {
		$boucle->where[] = $lang;
		$boucle->where[] = array("'NOT'", $origine_traduction);
		
		/* // environ equivalent
		$boucle->where[] = array("'IN'", "'$boucle->id_table." .
			"$_id_trad'", "'('.sql_get_select( 'strad.$primary', '$table_sql AS strad',
				'strad.$_id_trad=0 OR strad.$_id_trad = strad.$primary').')'");
		*/
	}
}
}
