<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Vortaro - Dictionnaire et traductions
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Vortaro\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function vortaro_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['vortoj'] = 'vortoj';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function vortaro_declarer_tables_objets_sql($tables) {

	$tables['spip_vortoj'] = array(
		'type' => 'vorto',
		'principale' => "oui", 
		'table_objet_surnoms' => array('vorto'), // table_objet('vorto') => 'vortoj' 
		'field'=> array(
			"id_vorto"           => "bigint(21) NOT NULL",
			"titre"              => "tinytext NOT NULL DEFAULT ''",
			"definition"         => "mediumtext NOT NULL DEFAULT ''",
			"avant"              => "tinytext NOT NULL DEFAULT ''",
			"lang"               => "VARCHAR(10) NOT NULL DEFAULT ''",
			"langue_choisie"     => "VARCHAR(3) DEFAULT 'non'", 
			"id_trad"            => "bigint(21) NOT NULL DEFAULT 0", 
			"maj"                => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY"        => "id_vorto",
			"KEY lang"           => "lang", 
			"KEY id_trad"        => "id_trad", 
		),
		'titre' => "titre AS titre, lang AS lang",
		 #'date' => "",
		'champs_editables'  => array('titre', 'definition', 'avant'),
		'champs_versionnes' => array('titre', 'definition', 'avant'),
		'rechercher_champs' => array("titre" => 5),
		'tables_jointures'  => array(),
		

	);

	return $tables;
}



?>